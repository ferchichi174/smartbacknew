const jwt = require('jsonwebtoken');
const jwt_config = require("../config/constant.config");
const constant = require('../config/constant.var');
const db = require("../model");





exports.verifyAll = (req, res, next) => {
    const token = req.header('sb-tkn');

    if (!token)
        return res.status(401).send({ message: "Access-Denied" });

    try {
        const verified = jwt.verify(token, jwt_config.secret);
        req.user = verified;
        next();

    } catch (error) {
        res.status(401).send({ message: "invalid-token" });
    }
};

exports.verifyRoot = (req, res, next) => {
    const token = req.header('sb-tkn');

    if (!token)
        return res.status(401).send({ message: "Access-Denied" });

    try {
        const verified = jwt.verify(token, jwt_config.secret);
        if (verified.role == constant.userRole.ROOT) {
            req.user = verified;
            next();
        } else {
            return res.status(401).send({ message: "Permission-Denied" });
        }

    } catch (error) {
        res.status(401).send({ message: "invalid-token" });
    }
};

exports.verifyAdmin = (req, res, next) => {
    const token = req.header('sb-tkn');

    if (!token)
        return res.status(401).send({ message: "Access-Denied" });

    try {
        const verified = jwt.verify(token, jwt_config.secret);
        if (verified.role == constant.userRole.ROOT || verified.role == constant.userRole.ADM) {
            req.user = verified;
            next();
        } else {
            return res.status(401).send({ message: "Permission-Denied" });
        }

    } catch (error) {
        res.status(401).send({ message: "invalid-token" });
    }
};

exports.verifyClient = async (req, res, next) => {
    const token = req.header('sb-tkn');

    if (!token)
        return res.status(401).send({ message: "Access-Denied" });

    try {
        const verified = jwt.verify(token, jwt_config.secret);
        // if (verified.role == constant.userRole.ROOT || verified.role == constant.userRole.ADM || verified.role == constant.userRole.CLIENT) {
        if (verified.role == constant.userRole.CLIENT) {
            let user = await db.User.findOne({ where: { id: verified.id } })
            if (user.etat == constant.userState.BLOCKD) {
                return res.status(401).send({ message: "Votre compte à été blocker , contacter l'administraur." });
            }
            req.user = verified;
            next();
        } else {
            return res.status(401).send({ message: "Permission-Denied" });
        }

    } catch (error) {
        res.status(401).send({ message: "invalid-token" });
    }
};

exports.verifyCondidat = async (req, res, next) => {
    const token = req.header('sb-tkn');

    if (!token)
        return res.status(401).send({ message: "Access-Denied" });

    try {
        const verified = jwt.verify(token, jwt_config.secret);
        // if (verified.role == constant.userRole.ROOT || verified.role == constant.userRole.ADM || verified.role == constant.userRole.CONDIDAT) {
        if (verified.role == constant.userRole.CONDIDAT) {
            let user = await db.User.findOne({ where: { id: verified.id } })
            if (user.etat == constant.userState.BLOCKD) {
                return res.status(401).send({ message: "Votre compte à été blocker , contacter l'administraur." });
            }
            req.user = verified;
            next();
        } else {
            return res.status(401).send({ message: "Permission-Denied" });
        }

    } catch (error) {
        res.status(401).send({ message: "invalid-token" });
    }
};

exports.verifyisActif = async (req, res, next) => {
    const token = req.header('sb-tkn');

    if (!token)
        return res.status(401).send({ message: "Access-Denied" });

    try {
        const verified = jwt.verify(token, jwt_config.secret);
        let user = await db.User.findOne({ where: { id: verified.id } })
        if (user.etat != constant.userState.ACTIVE) {
            return res.status(401).send({ message: "Votre compte n'est pas encore activé  , contactez nous pour activer votre compte." });
        }
        req.user = verified;
        next();

    } catch (error) {
        res.status(401).send({ message: "invalid-token" });
    }
};