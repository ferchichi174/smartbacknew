const db = require("./model");
const gst_user = db.User;
const constant = require('./config/constant.var');
const jwt = require('jsonwebtoken');
const jwt_config = require("./config/constant.config");
const mailer = require('./controller/Mail.controller');
const readline = require('readline');

let exit = false;

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const q_continue = () => {
    return new Promise((resolve, reject) => {
        rl.question('voulez vous ajouter une autre adresse mail (y/n) ?', (resp) => {
            console.log(`${resp}`)

            if (resp == 'y' || resp == 'Y') {
                //resolve(answer)
                resolve(resp)
            } else {
                rl.close();
                process.exit(1);
            }

        })
    })
}

const q_mail = () => {
    return new Promise((resolve, reject) => {
        rl.question('user email ? ', (answer) => {
            console.log(`${answer}`)
            //resolve(answer)

            gst_user.findOne({ where: { email: answer } }).then(data => {

                if (!data) {
                    console.log('utilisateur non existant !');
                    rl.close();
                    process.exit(1);
                } else {
                    console.log(`${data.etat}`)
                    const token = jwt.sign(
                        {
                            id: data.id,
                            role: data.role,
                            etat: data.etat,
                            nom: data.nom,
                            prenom: data.prenom,
                        },
                        jwt_config.secret, {},
                    );

                    let url = "";
                    if (data.role == constant.userRole.CLIENT) {
                        url = constant.base_url + 'register/vfcl/' + token
                    } else if (data.role == constant.userRole.CONDIDAT) {
                        url = constant.base_url + 'register/vfcd/' + token
                    }

                    mailer.sendMail(data.email, url)
                        .then((result) => {
                            console.log('Email sent...', result)
                            resolve(true)

                        })
                        .catch((error) => {
                            console.log(error.message);
                        });
                }

            }).catch((error) => {
                console.log(error.message);
                rl.close();
                process.exit(1);
            });
        })
    })
}


function main() {
    q_continue().then(data => q_mail().then(data => main()))
}

main()


