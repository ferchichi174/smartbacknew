module.exports = (sequelize, Sequelize) => {


    const experience = sequelize.define("experience", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true,
        },
        fonction: {
            type: Sequelize.STRING,
            allowNull: false
        },
        domaine: {
            type: Sequelize.STRING,
            allowNull: false
        },
        tech: {
            type: Sequelize.JSON,
            allowNull: false
        },
        lieu: {
            type: Sequelize.STRING,
            allowNull: false
        },
        entreprise: {
            type: Sequelize.STRING,
            allowNull: false
        },
        description: {
            type: Sequelize.TEXT,
            allowNull: false
        },
        date_debut: {
            type: Sequelize.DATEONLY,
            allowNull: false
        },
        date_fin: {
            type: Sequelize.DATEONLY,
            allowNull: false
        },
        actuel: {
            type: Sequelize.BOOLEAN,
            allowNull: false ,
            defaultValue: false
        },



    });






    return experience;
};