module.exports = (sequelize, Sequelize) => {


    const client = sequelize.define("client", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true,
        },
        type_client: {
            type: Sequelize.ENUM,
            values: ['esn', 'client'],
            allowNull: false
        },
        nom_entreprise: {
            type: Sequelize.STRING,
        },
        date_creation: {
            type: Sequelize.DATEONLY,
        },
        resume: {
            type: Sequelize.TEXT,
            allowNull: true
        },
        secteur: {
            type: Sequelize.STRING,
            allowNull: false
        },
        site_web: {
            type: Sequelize.STRING,
        },



    });






    return client;
};