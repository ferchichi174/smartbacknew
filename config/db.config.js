const varsConfig = require("./constant.config");
module.exports = {
    HOST: varsConfig.host,
    USER: varsConfig.db_user,
    PASSWORD: varsConfig.db_pass,
    DB: varsConfig.db,
    dialect: "mysql",
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    }
  };

  