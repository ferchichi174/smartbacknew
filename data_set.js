const { Condidat } = require("./model")

exports.users= [
    {
        email:"condi01@gmail.com" , 
        password :"123456" , 
        role :"condidat" , 
        etat:"active",
    condidat:{
        "nom": "Daniel",
        "prenom": "Colombus",
        "img":"http://localhost:5000/api/download/user4.jpg",
        "sex": "male",
        "date_naissance": "1990-07-25",
        "resume": "ingénieur informatique possédant une expérience pratique approfondie dans le développement des solutions web et mobiles .",
        "niv_etude": "bac+5",
        "niv_exp": "4",
        "titre": "Développeur Front-end mobile",
        "skills" : ["android ","java" ,"dev mobile","ios","swift","react native","kotlin"],
        "dispo" : "2021-07-01" ,
        "experiences": [{
            "domaine": "It    ",
            "fonction": "Dveloppeur Mobile",
            "entreprise": "Mntst tech",
            "lieu": "Tunisie",
            "description": "Developpeur et la realisation d'une application mobile ios en swift",
            "tech": {"skills" : ["android ","java" ,"dev mobile"]},
            "date_debut": "2021-07-10",
            "date_fin": "2021-07-24"
        }],
        "formations":[
            {
                "nom": "Ingénierie SIM",
                "ecole": "ESPRIT",
                "date_debut": "2010-07-01",
                "date_fin": "2021-07-31"
            },
            {
                "nom": "Bac Math",
                "ecole": "Lyce R2R",
                "date_debut": "2010-07-01",
                "date_fin": "2021-07-31"
            }
        ]
    }
    } ,

    {
        email:"condi02@gmail.com" , 
        password :"123456" , 
        role :"condidat" , 
        etat:"active",
    condidat:{
        "nom": "Daniel",
        "prenom": "Colombus",
        "img":"http://localhost:5000/api/download/user4.jpg",
        "sex": "male",
        "date_naissance": "1990-07-25",
        "resume": "ingénieur informatique possédant une expérience pratique approfondie dans le développement des solutions web et mobiles .",
        "niv_etude": "bac+5",
        "niv_exp": "4",
        "titre": "Développeur Front-end mobile",
        "skills" : ["android ","java" ,"dev mobile","ios","swift","react native","kotlin"],
        "dispo" : "2021-07-01" ,
        "experiences": [{
            "domaine": "It    ",
            "fonction": "Dveloppeur Mobile",
            "entreprise": "Mntst tech",
            "lieu": "Tunisie",
            "description": "Developpeur et la realisation d'une application mobile ios en swift",
            "tech": {"skills" : ["android ","java" ,"dev mobile"]},
            "date_debut": "2021-07-10",
            "date_fin": "2021-07-24"
        }],
        "formations":[
            {
                "nom": "Ingénierie SIM",
                "ecole": "ESPRIT",
                "date_debut": "2010-07-01",
                "date_fin": "2021-07-31"
            },
            {
                "nom": "Bac Math",
                "ecole": "Lyce R2R",
                "date_debut": "2010-07-01",
                "date_fin": "2021-07-31"
            }
        ]
    }
    } ,

    {
        email:"condi03@gmail.com" , 
        password :"123456" , 
        role :"condidat" , 
        etat:"active",
    condidat:{
        "nom": "Daniel",
        "prenom": "Colombus",
        "img":"http://localhost:5000/api/download/user4.jpg",
        "sex": "male",
        "date_naissance": "1990-07-25",
        "resume": "ingénieur informatique possédant une expérience pratique approfondie dans le développement des solutions web et mobiles .",
        "niv_etude": "bac+5",
        "niv_exp": "4",
        "titre": "Développeur Front-end mobile",
        "skills" : ["android ","java" ,"dev mobile","ios","swift","react native","kotlin"],
        "dispo" : "2021-07-01" ,
        "experiences": [{
            "domaine": "It    ",
            "fonction": "Dveloppeur Mobile",
            "entreprise": "Mntst tech",
            "lieu": "Tunisie",
            "description": "Developpeur et la realisation d'une application mobile ios en swift",
            "tech": {"skills" : ["android ","java" ,"dev mobile"]},
            "date_debut": "2021-07-10",
            "date_fin": "2021-07-24"
        }],
        "formations":[
            {
                "nom": "Ingénierie SIM",
                "ecole": "ESPRIT",
                "date_debut": "2010-07-01",
                "date_fin": "2021-07-31"
            },
            {
                "nom": "Bac Math",
                "ecole": "Lyce R2R",
                "date_debut": "2010-07-01",
                "date_fin": "2021-07-31"
            }
        ]
    }
    } ,

    {
        email:"condi04@gmail.com" , 
        password :"123456" , 
        role :"condidat" , 
        etat:"active",
    condidat:{
        "nom": "Daniel",
        "prenom": "Colombus",
        "img":"http://localhost:5000/api/download/user4.jpg",
        "sex": "male",
        "date_naissance": "1990-07-25",
        "resume": "ingénieur informatique possédant une expérience pratique approfondie dans le développement des solutions web et mobiles .",
        "niv_etude": "bac+5",
        "niv_exp": "4",
        "titre": "Développeur Front-end mobile",
        "skills" : ["android ","java" ,"dev mobile","ios","swift","react native","kotlin"],
        "dispo" : "2021-07-01" ,
        "experiences": [{
            "domaine": "It    ",
            "fonction": "Dveloppeur Mobile",
            "entreprise": "Mntst tech",
            "lieu": "Tunisie",
            "description": "Developpeur et la realisation d'une application mobile ios en swift",
            "tech": {"skills" : ["android ","java" ,"dev mobile"]},
            "date_debut": "2021-07-10",
            "date_fin": "2021-07-24"
        }],
        "formations":[
            {
                "nom": "Ingénierie SIM",
                "ecole": "ESPRIT",
                "date_debut": "2010-07-01",
                "date_fin": "2021-07-31"
            },
            {
                "nom": "Bac Math",
                "ecole": "Lyce R2R",
                "date_debut": "2010-07-01",
                "date_fin": "2021-07-31"
            }
        ]
    }
    } ,

    {
        email:"condi05@gmail.com" , 
        password :"123456" , 
        role :"condidat" , 
        etat:"active",
    condidat:{
        "nom": "Daniel",
        "prenom": "Colombus",
        "img":"http://localhost:5000/api/download/user4.jpg",
        "sex": "male",
        "date_naissance": "1990-07-25",
        "resume": "ingénieur informatique possédant une expérience pratique approfondie dans le développement des solutions web et mobiles .",
        "niv_etude": "bac+5",
        "niv_exp": "4",
        "titre": "Développeur Front-end mobile",
        "skills" : ["android ","java" ,"dev mobile","ios","swift","react native","kotlin"],
        "dispo" : "2021-07-01" ,
        "experiences": [{
            "domaine": "It    ",
            "fonction": "Dveloppeur Mobile",
            "entreprise": "Mntst tech",
            "lieu": "Tunisie",
            "description": "Developpeur et la realisation d'une application mobile ios en swift",
            "tech": {"skills" : ["android ","java" ,"dev mobile"]},
            "date_debut": "2021-07-10",
            "date_fin": "2021-07-24"
        }],
        "formations":[
            {
                "nom": "Ingénierie SIM",
                "ecole": "ESPRIT",
                "date_debut": "2010-07-01",
                "date_fin": "2021-07-31"
            },
            {
                "nom": "Bac Math",
                "ecole": "Lyce R2R",
                "date_debut": "2010-07-01",
                "date_fin": "2021-07-31"
            }
        ]
    }
    } ,

    {
        email:"condi06@gmail.com" , 
        password :"123456" , 
        role :"condidat" , 
        etat:"active",
    condidat:{
        "nom": "Daniel",
        "prenom": "Colombus",
        "img":"http://localhost:5000/api/download/user4.jpg",
        "sex": "male",
        "date_naissance": "1990-07-25",
        "resume": "ingénieur informatique possédant une expérience pratique approfondie dans le développement des solutions web et mobiles .",
        "niv_etude": "bac+5",
        "niv_exp": "4",
        "titre": "Développeur Front-end mobile",
        "skills" : ["android ","java" ,"dev mobile","ios","swift","react native","kotlin"],
        "dispo" : "2021-07-01" ,
        "experiences": [{
            "domaine": "It    ",
            "fonction": "Dveloppeur Mobile",
            "entreprise": "Mntst tech",
            "lieu": "Tunisie",
            "description": "Developpeur et la realisation d'une application mobile ios en swift",
            "tech": {"skills" : ["android ","java" ,"dev mobile"]},
            "date_debut": "2021-07-10",
            "date_fin": "2021-07-24"
        }],
        "formations":[
            {
                "nom": "Ingénierie SIM",
                "ecole": "ESPRIT",
                "date_debut": "2010-07-01",
                "date_fin": "2021-07-31"
            },
            {
                "nom": "Bac Math",
                "ecole": "Lyce R2R",
                "date_debut": "2010-07-01",
                "date_fin": "2021-07-31"
            }
        ]
    }
    } ,

    {
        email:"condi07@gmail.com" , 
        password :"123456" , 
        role :"condidat" , 
        etat:"active",
    condidat:{
        "nom": "Daniel",
        "prenom": "Colombus",
        "img":"http://localhost:5000/api/download/user4.jpg",
        "sex": "male",
        "date_naissance": "1990-07-25",
        "resume": "ingénieur informatique possédant une expérience pratique approfondie dans le développement des solutions web et mobiles .",
        "niv_etude": "bac+5",
        "niv_exp": "4",
        "titre": "Développeur Front-end mobile",
        "skills" : ["android ","java" ,"dev mobile","ios","swift","react native","kotlin"],
        "dispo" : "2021-07-01" ,
        "experiences": [{
            "domaine": "It    ",
            "fonction": "Dveloppeur Mobile",
            "entreprise": "Mntst tech",
            "lieu": "Tunisie",
            "description": "Developpeur et la realisation d'une application mobile ios en swift",
            "tech": {"skills" : ["android ","java" ,"dev mobile"]},
            "date_debut": "2021-07-10",
            "date_fin": "2021-07-24"
        }],
        "formations":[
            {
                "nom": "Ingénierie SIM",
                "ecole": "ESPRIT",
                "date_debut": "2010-07-01",
                "date_fin": "2021-07-31"
            },
            {
                "nom": "Bac Math",
                "ecole": "Lyce R2R",
                "date_debut": "2010-07-01",
                "date_fin": "2021-07-31"
            }
        ]
    }
    } ,

    {
        email:"condi08@gmail.com" , 
        password :"123456" , 
        role :"condidat" , 
        etat:"active",
    condidat:{
        "nom": "Daniel",
        "prenom": "Colombus",
        "img":"http://localhost:5000/api/download/user4.jpg",
        "sex": "male",
        "date_naissance": "1990-07-25",
        "resume": "ingénieur informatique possédant une expérience pratique approfondie dans le développement des solutions web et mobiles .",
        "niv_etude": "bac+5",
        "niv_exp": "4",
        "titre": "Développeur Front-end mobile",
        "skills" : ["android ","java" ,"dev mobile","ios","swift","react native","kotlin"],
        "dispo" : "2021-07-01" ,
        "experiences": [{
            "domaine": "It    ",
            "fonction": "Dveloppeur Mobile",
            "entreprise": "Mntst tech",
            "lieu": "Tunisie",
            "description": "Developpeur et la realisation d'une application mobile ios en swift",
            "tech": {"skills" : ["android ","java" ,"dev mobile"]},
            "date_debut": "2021-07-10",
            "date_fin": "2021-07-24"
        }],
        "formations":[
            {
                "nom": "Ingénierie SIM",
                "ecole": "ESPRIT",
                "date_debut": "2010-07-01",
                "date_fin": "2021-07-31"
            },
            {
                "nom": "Bac Math",
                "ecole": "Lyce R2R",
                "date_debut": "2010-07-01",
                "date_fin": "2021-07-31"
            }
        ]
    }
    } 

]
    
