module.exports = app => {
    const user_rep = require("../controller/User.controller.js");
    const verify = require("../midleware/TokenCheck");
    var router = require("express").Router();

    router.post("/newAdm", verify.verifyRoot ,user_rep.registerAdm);
    router.post("/newClient",user_rep.registerClient);
    router.post("/newCondidat", user_rep.registerCondidat);
    router.post("/login",user_rep.login);
    router.post("/checkuser", verify.verifyAll ,user_rep.checkUser);
    router.post("/validate", verify.verifyAdmin ,user_rep.validateUser);
    router.post("/invalidate", verify.verifyAdmin ,user_rep.inValidateUser);
    router.post("/changepass", verify.verifyAll ,user_rep.changePass);

    router.post("/rsvmail", verify.verifyAll ,user_rep.reSendVerifMail);
    router.post("/vrfmail", verify.verifyAll ,user_rep.verifMail);
    router.post("/checkprofile", verify.verifyAll ,user_rep.checkUserProfile);
    router.get("/checkcv", verify.verifyAll ,user_rep.checkCvUploaded);
    router.post("/refreshToken", verify.verifyAll ,user_rep.refreshToken);

    router.post("/resetPass", verify.verifyAll ,user_rep.resetPass);
    router.post("/forgetPass" , user_rep.forgetPass);

    router.post("/ban", verify.verifyAdmin ,user_rep.banUser);
    router.post("/unban", verify.verifyAdmin ,user_rep.unbanUser);
   

    app.use('/api/users',router);
};