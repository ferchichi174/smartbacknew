module.exports = app => {
    const rep = require("../controller/Cond.contoller");
    const verify = require("../midleware/TokenCheck");
    var router = require("express").Router();

    router.post("/newCondinf", verify.verifyCondidat ,rep.newCondInfo);
    router.post("/getUserCondInf", verify.verifyCondidat ,rep.getCondtByUser);
    
    router.post("/getCondList", verify.verifyClient , verify.verifyisActif ,rep.getAllCond);
    router.post("/searchmatch" ,  verify.verifyClient , verify.verifyisActif ,rep.searchMatch);
    router.post("/postuler" , verify.verifyCondidat , verify.verifyisActif ,rep.newDmdOffre);
    router.post("/topoffres" ,verify.verifyCondidat ,rep.getOffreOverview);
    router.post("/offres" ,verify.verifyCondidat ,rep.getNewOffres);
    router.post("/searchOffre" ,verify.verifyCondidat ,rep.searchOffre);
    router.post("/offreInf" ,verify.verifyCondidat ,rep.getOffreInfo);
    router.post("/getoverview" ,verify.verifyCondidat ,rep.getCondOverview);
    router.post("/myprofile" ,verify.verifyCondidat ,rep.getCondProfile);

    router.post("/setCondForm", verify.verifyCondidat ,rep.setCondFormations);
    router.post("/setCondExp", verify.verifyCondidat ,rep.setCondExp);

    router.post("/updateCondForm", verify.verifyCondidat ,rep.updateCondFormations);
    router.post("/updateCondExp", verify.verifyCondidat ,rep.updateCondExp);
    router.post("/updateCondInfo", verify.verifyCondidat ,rep.updateCondInfo);
    router.post("/updateUseInfo", verify.verifyCondidat ,rep.updateUseInfo);

    router.post("/getMyNotif", verify.verifyCondidat ,rep.getMyNotif);
    router.post("/setNotifRead", verify.verifyCondidat ,rep.setNotifRead);
    router.post("/getMyDmdOffer", verify.verifyCondidat ,rep.getMyDmdOffer);


    app.use('/api/cd',router);
};