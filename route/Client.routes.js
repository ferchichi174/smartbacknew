module.exports = app => {
    const rep = require("../controller/Client.controller");
    const verify = require("../midleware/TokenCheck");
    var router = require("express").Router();

    router.post("/newclinf", verify.verifyClient ,rep.newClientInfo);
    router.post("/getUserClientInf", verify.verifyClient ,rep.getClientByUser);
    router.post("/searchmatch" ,verify.verifyClient , verify.verifyisActif, rep.searchMatch);
    router.post("/newDmdCond" ,verify.verifyClient , verify.verifyisActif, rep.newDmdCond);
    router.post("/createOffre" ,verify.verifyClient , verify.verifyisActif, rep.createOffre);
    router.post("/getMyOffre" ,verify.verifyClient , rep.getMyOffre);
    router.post("/searchMyOffre" ,verify.verifyClient , rep.searchMyOffre);
    router.post("/saveOffre" ,verify.verifyClient , verify.verifyisActif, rep.updateOffre);
    router.post("/cancelOffre" ,verify.verifyClient , verify.verifyisActif, rep.deleteOffre);
    router.post("/offreinfo" ,verify.verifyClient , rep.getOffreInfo);
    router.post("/myprofile" ,verify.verifyClient , rep.getClientProfile);
    router.post("/edtmyprofile" ,verify.verifyClient , rep.updateClientProfile);
    router.post("/getoverview" ,verify.verifyClient , rep.getClientOverview);
    router.post("/getMissions" ,verify.verifyClient , rep.getMissions);
    router.post("/createFavoris" ,verify.verifyClient , rep.createFavoris);
    router.post("/deleteFavoris" ,verify.verifyClient , rep.deleteFavoris);
    router.post("/getFavorisRefs" ,verify.verifyClient , rep.getFavorisRefs);
    router.post("/getFavoris" ,verify.verifyClient , rep.getFavoris);
    router.post("/getMyNotif" ,verify.verifyClient , rep.getMyNotif);
    router.post("/setNotifRead" ,verify.verifyClient , rep.setNotifRead);


    app.use('/api/cl',router);
};