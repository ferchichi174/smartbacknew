module.exports = app => {
    const rep = require("../controller/Adm.controller");
    const verify = require("../midleware/TokenCheck");
    var router = require("express").Router();

   
    router.post("/searchmatchcond" ,verify.verifyAdmin , rep.searchMatchCond);
    router.post("/getAllCond" ,verify.verifyAdmin , rep.getAllCond);
    router.post("/getAllClient" ,verify.verifyAdmin , rep.getAllClient);
    router.post("/getAllDmd" ,verify.verifyAdmin , rep.getAllDmd);
    router.post("/getOffres" ,verify.verifyAdmin , rep.getOffres);
    router.post("/searchOffre" ,verify.verifyAdmin , rep.searchOffre);
    router.post("/getOffreInfo" ,verify.verifyAdmin , rep.getOffreInfo);
    router.post("/closeDmdOk" ,verify.verifyAdmin , rep.closeDmdSuccess);
    router.post("/closeDmdFail" ,verify.verifyAdmin , rep.closeDmdFail);

    router.post("/getMissions" ,verify.verifyAdmin , rep.getMissions);
    router.post("/updateMission" ,verify.verifyAdmin , rep.updateMission);
    router.post("/delMission" ,verify.verifyAdmin , rep.deleteMission);
    router.post("/changeMission" ,verify.verifyAdmin , rep.changeMissionEtat);
    router.post("/getMyNotif" ,verify.verifyAdmin , rep.getMyNotif);
    router.post("/setNotifRead" ,verify.verifyAdmin , rep.setNotifRead);
    router.post("/setCondTjm" ,verify.verifyAdmin , rep.setCondTjm);

    router.post("/editoffre" ,verify.verifyAdmin , rep.updateOffre);
    router.get("/contactus/list" ,verify.verifyAdmin , rep.getContactus);
    router.post("/contactus/new" , rep.createContactus);



    app.use('/api/adm',router);
};