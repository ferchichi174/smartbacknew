const db = require("../model");
const gst_msg = db.Message;
const constant = require('../config/constant.var');


exports.newMsg = (req, res) => {

    const msg = {
        content: req.body.content,
        read: false,
        id_from: req.user.id,
        id_to: req.body.id_to
    };

    const notif = {
        userId: req.body.id_to,
        content: 'vous avez reçu un <b>nouveau message</b>',
        url: '/dashb/chat',
        read: false,
        titre: 'msg'
    }

    gst_msg.create(msg).then(data => {
        db.Notif.create(notif).then(data_norif => {

            if (global.users[notif.userId])
                global.io.to(global.users[notif.userId]).emit("notif", 'new notif')

            if(!notif.userId)
            io.to("staff_room").emit("notif", 'new notif')

            res.send(data)
        })
            .catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while creating the notif"
                });
            });

    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the msg"
            });
        });

};

exports.getMyMsg = (req, res) => {

    const id_user = req.user.id;

    gst_msg.findAll({ where: { [db.Sequelize.Op.or]: [{ id_from: id_user }, { id_to: id_user }] }, }).then(data => {

        if (!data) {
            res.status(401).json({
                message: 'aucun message pour cette utilisateur !',
            });
        } else {
            res.send(data);

        }
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while try to login"
        });
    });

};

exports.getMsgByUser = (req, res) => {

    const id_user = req.body.id_user;


    gst_msg.findAll({ where: { [db.Sequelize.Op.or]: [{ id_from: id_user }, { id_to: id_user }] }, }).then(data => {

        if (!data) {
            res.status(401).json({
                message: 'aucun message pour cette utilisateur !',
            });
        } else {
            res.send(data);

        }
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while try to login"
        });
    });

};

