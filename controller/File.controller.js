const db = require("../model");
const gst_user = db.User;
const uploadFile = require("../midleware/upload");
const fs = require("fs");
const baseUrl = "https://smartbridge.fr/api/api/download/";
const path = require('path')

const upload = async (req, res) => {
  try {
    req.dir = ""
    await uploadFile(req, res);

    if (req.file == undefined) {
      return res.status(400).send({ message: "veuillez mettre votre photo !" });
    }
    let id_user = req.user.id ;
    gst_user.findOne({ where: { id: id_user } }).then(user => {

      if (!user) {
        res.status(401).json({
          message: 'user incorrect !',
        });
      } else {
        user.img = baseUrl + req.file.filename;
        user.save().then(rslt => {
          res.send({
            done: true,
            message: "Uploaded the file successfully: " + req.file.filename,
          })

        }).catch(err => {
          res.status(500).send({
            message: err.message || "Some error occurred while update colis"
          });
        });

      }
    }).catch(err => {
      res.status(500).send({
        message: err.message || "Some error occurred while try to login"
      });
    });


    // res.status(200).send({
    //   done: true,
    //   message: "Uploaded the file successfully: " + req.file.originalname,
    // });
  } catch (err) {
    console.log(err);

    if (err.code == "LIMIT_FILE_SIZE") {
      return res.status(500).send({
        message: "File size cannot be larger than 2MB!",
      });
    }

    if (err == "extension de fichier non autorisée !") {
      return res.status(400).send({
        message: "extension de fichier non autorisée !",
      });
    }

    res.status(500).send({
      message: `Could not upload the file !`,
    });
  }
};

const getListFiles = (req, res) => {
  const directoryPath = __basedir + "/uploads/";

  fs.readdir(directoryPath, function (err, files) {
    if (err) {
      res.status(500).send({
        message: "Unable to scan files!",
      });
    }

    let fileInfos = [];

    files.forEach((file) => {
      fileInfos.push({
        name: file,
        url: baseUrl + file,
      });
    });

    res.status(200).send(fileInfos);
  });
};

const download = (req, res) => {
  const fileName = req.params.name;
  const directoryPath = __basedir + "/uploads/";

  res.download(directoryPath + fileName, fileName, (err) => {
    if (err) {
      res.status(404).send({
        message: "Could not download the file. ",
      });
    }
  });
};

const download_cv = (req, res) => {
  const fileName = req.params.name;
  const directoryPath = __basedir + "/uploads/cv/";

  res.download(directoryPath + fileName, fileName, (err) => {
    if (err) {
      res.status(404).send({
        message: "Could not download the file. ",
      });
    }
  });
};

const upload_cv = async (req, res) => {
  try {
    req.dir = "cv/"
    await uploadFile(req, res);

    if (req.file == undefined) {
      return res.status(400).send({ message: "veuillez mettre votre cv !" });
    }
    if(path.extname(req.file.originalname) != ".doc" && path.extname(req.file.originalname) != ".docx" && path.extname(req.file.originalname) != ".pdf"){
      return res.status(400).send({ message: "Seuls les doc ou pdf sont autorisés !" });
    }
    

    let id_user = req.user.id ;
    gst_user.findOne({ where: { id: id_user } }).then(user => {

      if (!user) {
        res.status(401).json({
          message: 'user incorrect !',
        });
      } else {
        user.cv = baseUrl + "cv/"+ req.file.filename;
        user.save().then(rslt => {
          res.send({
            done: true,
            message: "fichier uploadé avec succès: " + req.file.filename,
          })

        }).catch(err => {
          res.status(500).send({
            message: err.message || "Some error occurred while update colis"
          });
        });

      }
    }).catch(err => {
      res.status(500).send({
        message: err.message || "Some error occurred while try to login"
      });
    });


    // res.status(200).send({
    //   done: true,
    //   message: "Uploaded the file successfully: " + req.file.originalname,
    // });
  } catch (err) {
    console.log(err);

    if (err.code == "LIMIT_FILE_SIZE") {
      return res.status(400).send({
        message: "La taille du fichier ne peut pas dépasser 2 Mo !",
      });
    }

    if (err == "extension de fichier non autorisée !") {
      return res.status(400).send({
        message: "extension de fichier non autorisée !",
      });
    }

    res.status(500).send({
      message: `Could not upload the file`,
    });
  }
};

module.exports = {
  upload,
  getListFiles,
  download,
  upload_cv,
  download_cv
};