const db = require("../model");
const gst_client = db.Client;
const gst_user = db.User;
const gst_cond = db.Condidat;
const gst_dmd = db.Demande_condidat;
const gst_offre = db.Offre;
const constant = require('../config/constant.var');
const jwt = require('jsonwebtoken');
const jwt_config = require("../config/constant.config");
const mailer = require('./Mail.controller');




exports.searchMatchCond = (req, res) => {

    let skills = req.body.skills;
    const niv_exp = req.body.niv_exp;

    //skills = skills.map(el => el.replace(`'`, `\\'`) )

    const atr_niv_exp_score = "( condidat.niv_exp REGEXP '" + niv_exp.join('|').toLowerCase() + "')";
    const atr_skills_score = "((LOWER(condidat.skills) LIKE '%" + skills.join("%') +(LOWER(condidat.skills) LIKE '%").toLowerCase() + "%'))";
    const atr_skills_tot = "(" + atr_niv_exp_score + " + " + atr_skills_score + " )";
    const atr_skills_where = "((LOWER(condidat.skills) LIKE '%" + skills.join("%') OR (LOWER(condidat.skills) LIKE '%").toLowerCase() + "%'))";



    gst_cond.findAll(
        {
            include: [db.Formation, db.Experience, { model: db.User, attributes: ['img'] }, db.Demande_condidat],
            attributes: [
                'id', 'date_naissance', 'sex', 'resume', 'skills', 'niv_etude', 'niv_exp', 'titre', 'dispo',
                [db.Sequelize.literal(atr_niv_exp_score), 'c1'],
                [db.Sequelize.literal(atr_skills_score), 'c2'],
                [db.Sequelize.literal(atr_skills_tot), 'tsc'],

            ],
            where: {
                [db.Sequelize.Op]: db.Sequelize.literal(atr_skills_where)

            },
            order: [db.Sequelize.literal('tsc DESC')]

        }).then(data => {

            if (!data) {
                res.status(204).json({
                    message: 'aucun client pour cette utilisateur !',
                });
            } else {
                res.send(data);

            }
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while try to login"
            });
        });

};

exports.getAllCond = (req, res) => {


    gst_cond.findAll({ include: [db.Formation, db.Experience, { model: db.User, attributes: ['img', 'nom', 'prenom', 'etat', 'email', 'id', 'tel', 'cv','tele'] }, db.Demande_condidat] }).then(data => {

        if (!data) {
            res.status(204).json({
                message: 'aucun client pour cette utilisateur !',
            });
        } else {
            res.send(data);

        }
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while try to login"
        });
    });

};

exports.getAllClient = (req, res) => {


    gst_client.findAll({ include: [{ model: db.User, attributes: ['img', 'nom', 'prenom', 'etat', 'email', 'id', 'tel'] }] }).then(data => {

        if (!data) {
            res.status(204).json({
                message: 'aucun client pour cette utilisateur !',
            });
        } else {
            res.send(data);

        }
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while try to login"
        });
    });

};

exports.getAllDmd = (req, res) => {


    gst_dmd.findAll({ include: [{ model: db.Condidat, include: 'user' }, { model: db.Client, include: 'user' }] }).then(data => {

        if (!data) {
            res.status(204).json({
                message: 'aucun client pour cette utilisateur !',
            });
        } else {
            res.send(data);


        }
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while try to login"
        });
    });

};

exports.getUserChat = (req, res) => {
    const id_user = req.user.id;

    gst_user.findAll({ where: { id: { [db.Sequelize.Op.not]: id_user } }, attributes: ['id', 'nom', 'prenom', 'etat', 'img', 'role'] }).then(data => {

        if (!data) {
            res.status(204).json({
                message: 'aucun client pour cette utilisateur !',
            });
        } else {
            res.send(data);

        }
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while try to login"
        });
    });

};

const getPagination = (page, size) => {
    const limit = size ? +size : 3;
    const offset = page ? page * limit : 0;

    return { limit, offset };
};

const getPagingData = (data, page, limit) => {
    const { count: totalItems, rows: offres } = data;
    const currentPage = page ? +page : 0;
    const totalPages = Math.ceil(totalItems / limit);

    return { totalItems, offres, totalPages, currentPage };
};

exports.getOffres = (req, res) => {


    let page = req.body.page;
    let size = req.body.size;
    const { limit, offset } = getPagination(page, size);

    gst_offre.findAndCountAll({ limit: limit, offset: offset, include: [db.Demande_offre, { model: db.Client, include: [{ model: db.User, attributes: ['img', 'nom', 'prenom', 'etat'] }] }], order: [['id', 'DESC']] }).then(data => {
        const response = getPagingData(data, page, limit);
        res.send(response)
    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while create offre"
            });
        });
};

exports.searchOffre = (req, res) => {

    const search = req.body.search;
    let page = req.body.page;
    let size = req.body.size;
    const { limit, offset } = getPagination(page, size);

    gst_offre.findAndCountAll({ where: { titre: { [db.Sequelize.Op.like]: `%${search}%` } }, limit: limit, offset: offset, include: [db.Demande_offre], order: [['id', 'DESC']] }).then(data => {
        const response = getPagingData(data, page, limit);
        res.send(response)
    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while create offre"
            });
        });
};


exports.getOffreInfo = (req, res) => {

    const id = req.body.id;

    gst_offre.findOne({ where: { id: id }, include: [{ model: db.Client, include: [{ model: db.User, attributes: ['img', 'nom', 'prenom', 'etat', 'email'] }] }, { model: db.Demande_offre, include: [{ model: db.Condidat, include: [db.Formation, db.Experience, db.Demande_condidat, { model: db.User, attributes: ['img', 'nom', 'prenom', 'etat', 'email'] }] }] }] }).then(data => {
        res.send(data)
    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while create offre"
            });
        });
};



exports.closeDmdSuccess = (req, res) => {

    const id_dmd = req.body.id_dmd;
    const mission = {
        date_debut: req.body.date_debut,
        date_fin: req.body.date_fin,
        tjm: req.body.tjm,
        etat: constant.missionState.EN_ATTENTE,
        clientId: req.body.clientId,
        condidatId: req.body.condidatId,

    };



    const notif = {
        userId: null,
        content: 'votre demande à bien été traité avec succès par l\'equipe Smart-Bridge , <b>une mission</b> à été programmer à ce propos',
        url: '/dashb/mission',
        read: false,
        titre: 'dmd'
    }

    gst_dmd.findOne({ where: { id: id_dmd } }).then(dmd => {
        if (!dmd) {

            res.status(400).send({
                message: "demande non trouvé !"
            });

        } else {

            dmd.etat = constant.dmdState.FERME_SUCCESS;
            dmd.save().then(dmd_updt => {

                db.Mission.create(mission).then(data => {
                    gst_client.findOne({ where: { id: mission.clientId } }).then(client => {
                        notif.userId = client.userId;



                        db.Notif.create(notif).then(data_norif => {

                            if (global.users[notif.userId])
                                global.io.to(global.users[notif.userId]).emit("notif", 'new notif')

                            res.send({ done: true, mission: data })

                        })
                            .catch(err => {
                                res.status(500).send({
                                    message: err.message || "Some error occurred "
                                });
                            });
                    })
                        .catch(err => {
                            res.status(500).send({
                                message: err.message || "Some error occurred "
                            });
                        });


                })
                    .catch(err => {
                        res.status(500).send({
                            message: err.message || "Some error occurred "
                        });
                    });
            })
                .catch(err => {
                    res.status(500).send({
                        message: err.message || "Some error occurred "
                    });
                });

        }

    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred !"
            });
        });
};

exports.closeDmdFail = (req, res) => {

    const id_dmd = req.body.id_dmd;
    const observation = req.body.observation;

    const notif = {
        userId: null,
        content: 'votre demande à bien été traité avec <b>echec</b> par l\'equipe Smart-Bridge ',
        url: '/dashb/',
        read: false,
        titre: 'dmd'
    }

    gst_dmd.findOne({ where: { id: id_dmd } }).then(dmd => {
        if (!dmd) {

            res.status(400).send({
                message: "demande non trouvé !"
            });

        } else {

            dmd.etat = constant.dmdState.FERME_FAIL;
            dmd.observation = observation;

            dmd.save().then(dmd_updt => {
                gst_client.findOne({ where: { id: dmd.clientId } }).then(client => {
                    notif.userId = client.userId;


                    db.Notif.create(notif).then(data_norif => {
                        if (global.users[notif.userId])
                            global.io.to(global.users[notif.userId]).emit("notif", 'new notif')

                        res.send({ done: true, data: dmd_updt })

                    })
                        .catch(err => {
                            res.status(500).send({
                                message: err.message || "Some error occurred "
                            });
                        });
                })
                    .catch(err => {
                        res.status(500).send({
                            message: err.message || "Some error occurred "
                        });
                    });


            })
                .catch(err => {
                    res.status(500).send({
                        message: err.message || "Some error occurred "
                    });
                });

        }

    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred !"
            });
        });
};

exports.getMissions = (req, res) => {

    db.Mission.findAll({ include: [{ model: db.Condidat, include: 'user' }, { model: db.Client, include: 'user' }], order: [['id', 'DESC']] }).then(data => {
        res.send(data)

    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred !"
            });
        });
};

exports.updateMission = (req, res) => {

    const mission = {
        id: req.body.id,
        tjm: req.body.tjm,
        date_debut: req.body.date_debut,
        date_fin: req.body.date_fin,
        etat: req.body.etat,
    };

    db.Mission.findByPk(mission.id).then(data => {

        if (!data) {
            res.status(204).send('mission introuvable !')
        } else {

            data.tjm = mission.tjm;
            data.date_debut = mission.date_debut;
            data.date_fin = mission.date_fin;
            data.etat = mission.etat;

            data.save().then(data_saved => {
                res.send({ done: true, data: data_saved })
            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while Edit mission"
                });
            });
        }

    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while finding offre"
            });
        });
};

exports.deleteMission = (req, res) => {

    const id = req.body.id;

    db.Mission.findByPk(id).then(data => {

        if (!data) {
            res.status(204).send('mission introuvable !')
        } else {

            data.destroy().then(data_del => {
                res.send({ done: true, data: data_del })
            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while delete mission"
                });
            });
        }

    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while delete mission"
            });
        });
};

exports.changeMissionEtat = (req, res) => {

    const id = req.body.id;
    const etat = req.body.etat;

    db.Mission.findByPk(id).then(data => {

        if (!data) {
            res.status(204).send('mission introuvable !')
        } else {

            data.etat = etat;

            data.save().then(data_saved => {
                res.send({ done: true, data: data_saved })
            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while Edit mission"
                });
            });
        }

    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while finding offre"
            });
        });
};

exports.getMyNotif = (req, res) => {

    db.Notif.findAll({
        where: { userId: { [db.Sequelize.Op.eq]: null } },
        order: [['id', 'DESC']]
    }).then(data => {
        res.send(data)

    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred !"
            });
        });
};

exports.setNotifRead = (req, res) => {

    db.Notif.update(
        { read: true },
        { where: { userId: { [db.Sequelize.Op.eq]: null }, read: false } }

    ).then(data => {
        res.send(data)

    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred !"
            });
        });
};

exports.setCondTjm = (req, res) => {

    const id = req.body.id;
    const tjm = req.body.tjm;

    db.Condidat.findByPk(id).then(data => {

        if (!data) {
            res.status(204).send('condidat introuvable !')
        } else {

            data.tjm = tjm;

            data.save().then(data_saved => {
                res.send({ done: true, data: data_saved })
            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred "
                });
            });
        }

    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred "
            });
        });
};


exports.updateOffre = (req, res) => {

    const id = req.body.id;
    const model = req.body;

    gst_offre.update(model, { where: { id: id } })
        .then(affectedRows => {
            gst_offre.findOne({ where: { id: id } }).then(obj => {
                res.send({done : true , offre : obj})
            }).catch(err =>  res.status(500).send(err.errors ? err.errors[0].message : err.message))
        })
        .catch(err =>  res.status(500).send(err.errors ? err.errors[0].message : err.message))
};


exports.getContactus = (req, res) => {

    db.Contactus.findAll({order: [['id', 'DESC']] }).then(data => {
        res.send(data)

    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred !"
            });
        });
};


exports.createContactus = (req, res) => {
    db.Contactus.create(req.body).then(created_user => {
        res.send({done : true})
    }).catch(err => res.status(500).send(err.errors ? err.errors[0].message : err.message));
};
